import { BaseDataService } from './base.data.service';
import { Issue } from '../../types/issue';
import { env } from '../../env/env';

export class IssueDataService extends BaseDataService<Issue> {

    constructor() {
        super(env.api.issues);
    }
}