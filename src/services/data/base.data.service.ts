import * as csvtojson from 'csvtojson';

export abstract class BaseDataService<T> {

    protected constructor(private url: string) {
    }

    getList(): Promise<T[]> {
        return new Promise(resolve => {
            csvtojson({ delimiter: ',' })
                .fromFile(this.url)
                .then(data => resolve(data));
        });
    }
}
