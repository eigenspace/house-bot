// @ts-ignore
import * as WebSocket from 'ws';

export function initSocketServer(port: number): void {
    const clients = new Map<number, WebSocket>();
    const webSocketServer = new WebSocket.Server({ port });

    webSocketServer.on('listening', () => console.log(`server is listening on port ${port}!`));
    webSocketServer.on('connection', (ws: WebSocket) => {
        const clientId = (new Date()).getTime();

        clients.set(clientId, ws);

        console.log(`new connection ${clientId}`);

        // @ts-ignore
        ws.on('message', message => {
            console.log(`received message: ${message}`);

            Array.from(clients.values())
                .forEach(client => client.send(message));
        });

        // @ts-ignore
        ws.on('close', message => {
            console.log(`connection has been closed ${message}`);
            clients.delete(clientId);
        });
    });

    // @ts-ignore
    return clients;
}
