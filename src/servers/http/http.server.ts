/* tslint:disable:no-magic-numbers max-line-length */
import * as express from 'express';
import { Response } from 'express';
import * as bodyParser from 'body-parser';
import { WebhookClient } from 'dialogflow-fulfillment/src/dialogflow-fulfillment';
import { IntentType } from '../../enums/intent-type.enum';
import { Logger } from '../../utils/logger/logger';
import { LoggingLevelType } from '../../utils/logger/logging-level-type.enum';
// @ts-ignore
import * as Telegram from 'telegraf/telegram';
import { env } from '../../env/env';
import { ExtraEditMessage } from 'telegraf/typings/telegram-types';
import { HttpStatus } from '../../enums/http-status.enum';
import { DemandCategory } from '../../enums/demand-category.enum';
import { DemandStatus } from '../../enums/demand-status.enum';
import { IssueDataService } from '../../services/data/issue.data.service';
import { Issue } from '../../types/issue';

// tslint:disable:no-any
export function initHttpServer(clients: Map<number, WebSocket>, port = 3040): void {
    const logger = new Logger({
        prefix: 'http server',
        logLevel: LoggingLevelType.DEBUG
    });

    const app = express();

    const nikichTgId = 337032096;
    const tohasanTgId = 192314783;
    const danyaTgId = 339052729;
    const userIds = new Set([nikichTgId, tohasanTgId, danyaTgId]);

    const telegram = new Telegram(env.token);
    const issuesService = new IssueDataService();

    app.use(async ({}, res, next) => {
        // Enable CORS
        res.set('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    });

    app.use(bodyParser.json({ limit: '10mb' }));
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/', about);

    app.get('/info', about);

    app.get('/send', (req, res) => {
        broadcastIssue({
            title: 'Прорвало трубу',
            description: 'У нас в доме перегорела лампочка на 5-ом этаже',
            category: DemandCategory.EMERGENCY
        } as Issue);

        res.sendStatus(HttpStatus.OK);
    });

    app.post('/news', (req, res) => {
        const body = req.body;
        const text = [
            `*${body.title}*`,
            '',
            body.description
        ].join('\n');

        userIds.forEach(userId => {
            if (!body.img) {
                telegram.sendMessage(userId, '', { text, parse_mode: 'Markdown' } as ExtraEditMessage);
                return;
            }

            telegram.sendPhoto(userId, body.img, { caption: text, parse_mode: 'Markdown' } as ExtraEditMessage);
        });

        res.sendStatus(HttpStatus.OK);
    });

    app.post('/sendMessage', (req, res) => {
        const text = req.body.text;

        userIds.forEach(userId => {
            telegram.sendMessage(userId, text);
        });

        res.sendStatus(HttpStatus.OK);
    });

    app.post('/sendMockMessage', (req, res) => {
        const issues = req.body.issues;

        let text = 'Я не понимаю о чем ты :( Опиши ситуацию подробнее ;)';

        switch (issues) {
            case 1:
                text = 'В течение 30 секунд будет перекрыта вода, так что не беспокойся! А уже через десять минут к тебе приедут на помощь.';
                break;
            case 2:
                text = 'Отключи электричество, НЕ открывай окна, постарайся, как можно скорее, покинуть помещение. К тебе уже едут на помощь.';
                break;
            case 3:
                text = 'Выбери удобное для тебя время, когда мог бы прийти мастер и помочь в этом непросто деле';
                break;
            case 4:
                text = 'Нет проблем, выбери удобное тебе время, когда мог бы прийти мастер и заменить его';
                break;
            case 5:
                text = 'Заявка принята, в ближайшие дни мы это поправим ;)';
                break;
            case 6:
                text = 'Приносим извинения, уже сегодня вечером наведём марафет! ;)';
                break;
        }

        userIds.forEach(userId => {
            telegram.sendMessage(userId, text);
        });

        res.sendStatus(HttpStatus.OK);
    });

    app.post('/fulfillment', async (request, response) => {
        const agent = new WebhookClient({ request, response });

        const userId = agent.originalRequest.payload.message.chat.id;
        userIds.add(userId);

        const intentMap = new Map([
            [IntentType.ACCIDENT, (client: WebhookClient) => chooseIssue(client)]
        ]);

        agent.handleRequest(intentMap);
    });

    app.listen(port, '0.0.0.0', () => logger.log(`service is listening on port ${port}!`));

    async function chooseIssue(agent: WebhookClient): Promise<void> {
        const issues = await issuesService.getList();
        const { issue: issueId } = agent.parameters;
        const issue = issues.find(item => item.id === issueId);

        if (issue) {
            broadcastIssue(issue);
        }

        agent.add(issue && issue.solution || '');
    }

    function broadcastIssue(issue: Issue): void {
        const author = { name: 'Большедворов Г. А.', phone: '+79645678964' };

        const demand = {
            id: Date.now(),
            date: new Date(),
            status: DemandStatus.UNPROCESSED,
            category: issue.category,
            content: issue.title,
            author
        };

        const message = { demand };
        const data = JSON.stringify({ type: 'demand', message });

        const clientList = Array.from(clients.values());
        clientList.forEach(client => client.send(data));
    }

    function about({}, res: Response): void {
        res.send('Привет! :) Меня зовут Люся! Я помогу тебе в решении любых вопросов, связанных с ЖКХ :)');
    }
}
