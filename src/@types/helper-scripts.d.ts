declare module '@eigenspace/helper-scripts' {

    export function parseProcessArgs(args: string[]): Map<string, string[]>;
}