declare module 'dialogflow-fulfillment/src/dialogflow-fulfillment' {
    // tslint:disable-next-line:no-any
    interface Dictionary<T = any> {
        [key: string]: T;
    }

    export class WebhookClient {
        originalRequest: Dictionary;
        parameters: Dictionary;

        constructor(props: Dictionary);

        handleRequest(handlers: Map<string, Function>): Promise<void>;

        add(response: Dictionary | Dictionary[] | string): void;
    }

    export class Card {
        constructor(options: Dictionary);
    }

    export class Payload {
        constructor(platform: string, options: Dictionary);
    }
}
