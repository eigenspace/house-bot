import { initHttpServer } from './servers/http/http.server';
import { initSocketServer } from './servers/socket/socket.server';

// tslint:disable-next-line
const instance = initSocketServer(3400);

// tslint:disable-next-line:no-any
initHttpServer(instance as any);
