// tslint:disable-next-line:no-any
export interface Dictionary<T = any> {
    [key: string]: T;
}