import { BaseEntity } from './base-entity';

export interface Issue extends BaseEntity {
    title: string;
    description?: string;
    category?: string;
    solution?: string;
}
