export interface BaseEntity {
    id: string;

    [key: string]: string | undefined;
}