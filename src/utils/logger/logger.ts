// tslint:disable:no-any
import { LoggingLevelType } from './logging-level-type.enum';
import { env } from '../../env/env';

/**
 * Logger wraps console methods.
 * It adds such bonuses like:
 *      - base prefix, for instance: [bp] ...
 *      - log level, for instance: DEBUG
 *      - prefix, for instance: ... [wa-client] ...
 *
 * author: aka
 * date: 03.10.2017.
 */
export class Logger {
    private static lastLoggingTimestamp: number = Date.now();

    basePrefix: string;
    logLevel: number;
    prefix: string;

    constructor(options: any = {}) {
        this.basePrefix = options.basePrefix || 'fc';
        this.logLevel = options.logLevel || (env.logLevel ? LoggingLevelType[env.logLevel] : LoggingLevelType.ERROR);
        this.prefix = options.prefix || '';
    }

    /**
     * Method wraps console.log() function.
     *
     * @param args Various array of arguments to log.
     */
    log(...args: any[]): void {
        this.invoke('log', LoggingLevelType.DEBUG, ...args);
    }

    /**
     * Method wraps console.info() function.
     *
     * @param args Various array of arguments to log.
     */
    info(...args: any[]): void {
        this.invoke('info', LoggingLevelType.INFO, ...args);
    }

    /**
     * Method wraps console.warn() function.
     *
     * @param args Various array of arguments to log.
     */
    warn(...args: any[]): void {
        this.invoke('warn', LoggingLevelType.WARNING, ...args);
    }

    /**
     * Method wraps console.error() function.
     *
     * @param args Various array of arguments to log.
     */
    error(...args: any[]): void {
        this.invoke('error', LoggingLevelType.ERROR, ...args);
    }

    private invoke(functionName: string, logLevel: number, ...args: any[]): void {
        if (logLevel <= this.logLevel) {
            const basePrefix = this.basePrefix ? `[${this.basePrefix}]` : '';
            const prefix = this.prefix ? `[${this.prefix}]` : '';
            const currentTime = new Date();
            const timePrefix = `[${currentTime.toISOString()}]`;
            const performancePrefix = `[${currentTime.getTime() - Logger.lastLoggingTimestamp} ms]`;
            Logger.lastLoggingTimestamp = currentTime.getTime();

            const fullPrefix = `${basePrefix}${prefix}${timePrefix}${performancePrefix}`;

            if (typeof args[0] === 'string') {
                args[0] = `${fullPrefix} ${args[0]}`;
            } else {
                Array.prototype.splice.call(args, 0, 0, fullPrefix);
            }

            (console as any)[functionName](...args);
        }
    }
}
