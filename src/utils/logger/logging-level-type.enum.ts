/**
 * author: aka
 * date: 03.10.2017.
 */
export const LoggingLevelType: { [key: string]: number } = {
    NONE: 0,
    ERROR: 1,
    WARNING: 2,
    INFO: 3,
    DEBUG: 4
};
