export enum DemandCategory {
    NORMAL = 'Неаварийная ситуация',
    EMERGENCY = 'Аварийная ситуация'
}
