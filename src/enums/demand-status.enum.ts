export enum DemandStatus {
    PROCESSED = 'processed',
    COMPLETE = 'complete',
    UNPROCESSED = 'unprocessed'
}
