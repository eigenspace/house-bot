import { DemandStatus } from '../enums/demand-status.enum';
import { DemandCategory } from '../enums/demand-category.enum';

export interface Demand {
    id: number;
    date: Date;
    status: DemandStatus;
    category: DemandCategory;
    content: string;
    issue: string;
    author: {
        name: string;
        phone: string;
    };
}
